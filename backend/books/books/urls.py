from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views

router = DefaultRouter()
router.register(r'books', views.BooksViewSet, basename='books')

urlpatterns = [
    path('', include(router.urls))
]