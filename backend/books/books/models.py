from django.db import models


class Author(models.Model):
    name = models.CharField(max_length=128, primary_key=True)


class Category(models.Model):
    name = models.CharField(max_length=128, primary_key=True)


class Book(models.Model):
    id = models.CharField(max_length=100, primary_key=True)
    title = models.CharField(max_length=128)
    authors = models.ManyToManyField(Author, related_name='books')
    subtitle = models.CharField(max_length=256, null=True, blank=True)
    publisher = models.CharField(max_length=128, null=True, blank=True)
    published_date = models.CharField(max_length=20, null=True, blank=True)
    description = models.TextField(max_length=4096, null=True, blank=True)
    page_count = models.IntegerField(null=True)
    categories = models.ManyToManyField(Category, related_name='books')
    image_link = models.URLField(max_length=2056, null=True, blank=True)
