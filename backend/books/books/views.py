import requests
from rest_framework import status, filters, mixins, viewsets
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend

from . import serializers
from . import models


DATASET_URL = 'https://www.googleapis.com/books/v1/volumes'


class BooksViewSet(mixins.CreateModelMixin,
                   mixins.RetrieveModelMixin,
                   mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    lookup_field = 'id'
    serializer_class = serializers.BookSerializer
    queryset = models.Book.objects.all()
    filter_backends = [filters.OrderingFilter, DjangoFilterBackend, ]
    filterset_fields = ['published_date', 'authors']
    ordering_fields = ['published_date', ]

    def create(self, request, *args, **kwargs):
        serializer = serializers.PostSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        data_query = serializer.data.get('q')
        r = requests.get(DATASET_URL, params={'q': data_query})
        if r.status_code == requests.codes.ok:

            items = r.json().get('items')

            for item in items:
                info = item.get('volumeInfo')
                authors = info.get('authors')
                categories = info.get('categories')

                authors_list = []
                for author in authors:
                    obj, _ = models.Author.objects.update_or_create(
                        name=author
                    )
                    authors_list.append(obj)

                categories_list = []
                if categories:
                    for category in categories:
                        obj, _ = models.Category.objects.update_or_create(
                            name=category
                        )
                        categories_list.append(obj)

                book, _ = models.Book.objects.update_or_create(
                    id=item.get('id'), defaults={
                        'title': info.get('title'),
                        'subtitle': info.get('subtitle'),
                        'publisher': info.get('publisher'),
                        'description': info.get('description'),
                        'published_date': info.get('publishedDate'),
                        'image_link': info.get('imageLinks', {})
                                        .get('thumbnail'),
                        'page_count': info.get('pageCount'),
                    })

                book.authors.set(authors_list)
                book.categories.set(categories_list)

            headers = self.get_success_headers(serializer.data)
            return Response(status=status.HTTP_201_CREATED, headers=headers)
        else:
            return Response(status=r.status_code)
