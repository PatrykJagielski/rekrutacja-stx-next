import React, { useEffect, useState } from "react"

function App() {
  const [books, setBooks] = useState([])
  const [input, setInput] = useState("")

  function fetchData() {
    fetch("https://api.patrykjagielski.pl/books")
      .then((response) => response.json())
      .then((data) => setBooks(data))
  }

  useEffect(() => {
    fetchData()
  }, [])

  const booksRender = books.map((book) => (
    <div style={{ margin: "10px" }}>{book.title}</div>
  ))

  function getDataSet() {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ q: input }),
    }
    fetch("https://api.patrykjagielski.pl/books/", requestOptions).then(
      (response) => {
        if (response.status === 201) {
          fetchData()
        }
      }
    )
  }

  return (
    <div>
      <label>dodaj set do zbioru: </label>
      <input onChange={(e) => setInput(e.target.value)} />
      <button onClick={getDataSet}>ADD</button>
      {booksRender}
    </div>
  )
}

export default App
