FROM python:3.7.8-alpine

COPY ./image/bin/runserver /usr/local/bin

RUN apk add --no-cache --update \
      bash \
      postgresql-client \
      postgresql-dev \
      libpq \
      musl-dev \
      gcc

COPY ./requirements.txt ./
RUN pip3 install -Ur requirements.txt

WORKDIR /app/src

VOLUME ["/app/src"]

EXPOSE 8000
